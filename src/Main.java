import javax.swing.*;

/*
 * 資管4A
 * 105403031  莫智堯
 */

public class Main extends HW6{
    public static void main(String[] args){
        HW6 HW6_drawer = new HW6();
        HW6_drawer.setSize(800, 700);
        HW6_drawer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        HW6_drawer.setVisible(true);
    }
}
